﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControladorJugador : MonoBehaviour {

	public Text puntuacion; 
	public Text win; 
    int contador;
	Rigidbody rb;
	public float velocidad = 10;

	public void OnTriggerEnter(Collider other){
		if (other.gameObject.CompareTag ("Pick Up")){
            other.gameObject.SetActive (false);
            contador += 1;
			actualizarMarcador();
        }
	}

	private void actualizarMarcador(){
		puntuacion.text = "Puntos: " + contador;
		if(contador >= 12){
			win.gameObject.SetActive(true);
		}
	}

	public void Awake(){
		rb = GetComponent<Rigidbody>();
		contador = 0;
		actualizarMarcador();
		win.gameObject.SetActive(false);
	}

	public void FixedUpdate(){
		float movimientoHorizontal = Input.GetAxis("Horizontal");
		float movimientoVertical = Input.GetAxis("Vertical");
		Vector3 movimiento = new Vector3(movimientoHorizontal, 0, movimientoVertical);
		rb.AddForce(movimiento * velocidad);
	}

}
